//*****************************************************************************
// Canvas Management
//
// Used for accessing the Game Container Element
//
// Required Systems:
//  * Engine
//
// Required libraries:
// * JQuery
//
// Features:
// * Canvas access:
// - this.ctx, or this.getCanvas()
// * Wrapper element access:
// - this.el
//
// @TODO, CSS (Chapter 15)
// - document options more clearly
// - support a border option
// - image rendering function could take an object with parameters, instead of
// a long list of parameters (to futher support more options like shadows)
// - image-rendering to control resampling method
//
// - setting fill & stroke styles, drawing lines, curves, rects
// - move canvas drawing into it's own module
//
// - rendering text
//
// - support for creating multiple canvases
// - shadow properties for drawing objects (or maybe not, they're expensive)
// - support for composite operations ()
//*****************************************************************************

// Requires Engine namespace
(function() {
	engineAssert( Engine, "No Engine Definition" );
})();


//*****************************************************************************
// Static objects/functions
//*****************************************************************************
var DEFAULT_ENGINE_ID = "defaultEngineID";
var DEFAULT_CANVAS_WIDTH = 800;
var DEFAULT_CANVAS_HEIGHT = 600;

//=========================================================================
// SetupCanvas - gets a handle to the render context
//
// accommodates different usage patterns:
// - called with no parameters
// - with an id of an element
// - with the element itself
//
// If it can't find an element, it creates a new <canvas> element
//
//=========================================================================
Engine.prototype.setupCanvas = function(id, options) {
	var touchDevice = 'ontouchstart' in document;
	options = options || {};
	id = id || DEFAULT_ENGINE_ID;

	this.el = $(_.isString(id) ? "#" + id : id);

	options.width = options.width || DEFAULT_CANVAS_WIDTH;
	options.height = options.height || DEFAULT_CANVAS_HEIGHT;

	if(this.el.length === 0) {
		this.el = $("<canvas width='" + options.width + "' height='" + options.height + "'></canvas>").attr('id',id).appendTo("body");
	}

	var maxWidth = options.maxWidth || 5000,
		maxHeight = options.maxHeight || 5000,
		resampleWidth = options.resampleWidth,
		resampleHeight = options.resampleHeight;

	if(options.maximize) {
		$("html, body").css({ padding:0, margin: 0 });
		var w = Math.min(window.innerWidth,maxWidth);
		var h = Math.min(window.innerHeight - 5,maxHeight);

		if(touchDevice) {
			this.el.css({height: h * 2});
			window.scrollTo(0,1);

			w = Math.min(window.innerWidth,maxWidth);
			h = Math.min(window.innerHeight - 5,maxHeight);
		}

		if(((resampleWidth && w > resampleWidth) ||
			(resampleHeight && h > resampleHeight)) &&
			touchDevice)
		{
			this.el.css({  width:w, height:h }).attr({ width:w/2, height:h/2 });
		}
		else
		{
			this.el.css({  width:w, height:h }).attr({ width:w, height:h });
		}
	}

	this.wrapper = this.el.wrap("<div id='" + id + "_container'/>")
				.parent()
				.css({ width: this.el.width(),
				margin: '0 auto' });

	this.el.css('position','relative');

	this.ctx = this.el[0].getContext && this.el[0].getContext("2d");


	if(touchDevice) {
		window.scrollTo(0,1);
	}
	this.width = parseInt(this.el.attr('width'),10);
	this.height = parseInt(this.el.attr('height'),10);

	$(window).bind('orientationchange',function() {
		setTimeout(function() { window.scrollTo(0,1); }, 0);
	});

	// returns the engine instance allowing it to be chained with further engine calls
	return this;
};

//=========================================================================
// Returns render context
//=========================================================================
Engine.prototype.getCanvas = function() {
	return this.ctx;
};

//=========================================================================
// Clears the entirety of the Canvas
//=========================================================================
Engine.prototype.clearCanvas = function() {
	this.ctx.clearRect(0,0,this.el[0].width,this.el[0].height);
};

//=========================================================================
// Renders a transformed image to the canvas at the given position
// posX, posY can be floating point, but will be floored to pixel coords
// angle is in radians
//=========================================================================
Engine.prototype.drawImage = function(image, posX, posY, width, height, angle, alpha) {

	width = width || image.width;
	height = height || image.height;
	angle = angle || 0;
	alpha = alpha || 1.0;

	//@TODO - enable support for this option
	// if( this.canvasFloorToPixels ) {
	// 	posX = Math.floor(posX);
	// 	posY = Math.floor(posY);
	// }

	// save the current co-ordinate system before we screw with it
	var ctx = this.ctx;
	ctx.save();

	ctx.globalAlpha = alpha;

	// move to the middle of where we want to draw our image
	ctx.translate( posX, posY );

	// rotate around that point
	ctx.rotate( angle );

	// draw it up and to the left by half the width and height of the image
	ctx.drawImage( image, -(width/2), -(height/2), width, height );

	// and restore the co-ords to how they were when we began
	// ctx.rotate( -angle );
	// ctx.translate( -posX, -posY );
	// ctx.globalAlpha = 1.0;
	ctx.restore();
};


//=========================================================================
// createPattern
// returns a pattern object
// repeatOption: "repeat", "repeat-x", "repeat-y", "no-repeat"
//=========================================================================
Engine.prototype.createPattern = function( image, repeatOption ) {
	return this.ctx.createPattern( image, repeatOption );
};

//=========================================================================
// Renders a transformed image to the canvas at the given position
// posX, posY can be floating point, but will be floored to pixel coords
// angle is in radians
//=========================================================================
Engine.prototype.drawPattern = function(pattern, posX, posY, width, height, angle, alpha ) {

	// save the current co-ordinate system before we screw with it
	var ctx = this.ctx;
	ctx.save();

	ctx.globalAlpha = alpha;

	// move to the middle of where we want to draw our image
	ctx.translate( posX, posY );

	// rotate around that point
	ctx.rotate( angle );

	// draw it up and to the left by half the width and height of the image
	ctx.rect( -(width/2), -(height/2), width, height);
	ctx.fillStyle = pattern;
	ctx.fill();

	// and restore the co-ords to how they were when we began
	// ctx.rotate( -angle );
	// ctx.translate( -posX, -posY );
	// ctx.globalAlpha = 1.0;
	ctx.restore();
};

//=========================================================================
// Renders a transformed image to the canvas at the given position
// posX, posY can be floating point, but will be floored to pixel coords
// angle is in radians
//=========================================================================
Engine.prototype.drawClippedImage = function(image, sx, sy, tilew, tileh, posX, posY, width, height, angle, alpha ) {

	width = width || tilew;
	height = height || tileh;

	// save the current co-ordinate system before we screw with it
	var ctx = this.ctx;
	ctx.save();

	ctx.globalAlpha = alpha;

	// move to the middle of where we want to draw our image
	ctx.translate( posX, posY );

	// rotate around that point
	ctx.rotate( angle );

	// draw it up and to the left by half the width and height of the image
	ctx.drawImage( image, sx, sy,
		tilew, tileh,
		-(width/2), -(height/2),
		width, height );

	// and restore the co-ords to how they were when we began
	// ctx.rotate( -angle );
	// ctx.translate( -posX, -posY );
	// ctx.globalAlpha = 1.0;
	ctx.restore();
};


//////////////////////////////////////////////
// setFont
// sets the font, size and style of text
//
Engine.prototype.setFont = function( font ) {
	this.ctx.font = font;
};

//////////////////////////////////////////////
// fillText
// * draws text filled solid color at given position
// optional parameters:
// * fillStyle: color string, hex value, or RGB value.
// * align: "start", "end", "left", "center", "right"
// * baseline: "top", "hanging", "alphabetic", "ideographic", "bottom"
//
Engine.prototype.fillText = function( text, posX, posY, fillStyle, align, baseline ) {
	var ctx = this.ctx;
	ctx.save();
	if( fillStyle ) {
		ctx.fillStyle = fillStyle;
	}
	if( align ) {
		ctx.textAlign = align;
	}
	if( baseline ) {
		ctx.textBaseline = baseline;
	}
	ctx.fillText( text, posX, posY );
	ctx.restore();
};

//////////////////////////////////////////////
// strokeText
// * draws text with just an outline at given position
// optional parameters:
// * strokeStyle: color string, hex value, or RGB value.
// * align: "start", "end", "left", "center", "right"
// * baseline: "top", "hanging", "alphabetic", "ideographic", "bottom"
//
Engine.prototype.strokeText = function( text, posX, posY, strokeStyle, align, baseline ) {
	var ctx = this.ctx;
	ctx.save();
	if( fillStyle ) {
		ctx.fillStyle = fillStyle;
	}
	if( align ) {
		ctx.textAlign = align;
	}
	if( baseline ) {
		ctx.textBaseline = baseline;
	}
	ctx.strokeText( text, posX, posY );
	ctx.restore();
};

//////////////////////////////////////////////
// drawLine:
// * Start Position to End Position
// optional parameters:
// * lineWidth: how thick is the line
// * strokeSytle: color in string ("blue"), a hex value (#FF0000), or rgb(255,0,0)
// * lineCap: "round", "butt", "square"
//
Engine.prototype.drawLine = function(startX, startY, endX, endY, lineWidth, strokeStyle, lineCap) {
	var ctx = this.ctx;
	ctx.save();
	ctx.beginPath();
	ctx.moveTo(startX,startY);
	ctx.lineTo(endX,endY);

	if( lineWidth ) {
		ctx.lineWidth = lineWidth;
	}
	if( strokeStyle ) {
		ctx.strokeStyle = strokeStyle;
	}
	if( lineCap ) {
		ctx.lineCap = lineCap;
	}
	ctx.stroke();
	ctx.restore();
};

//////////////////////////////////////////////
// drawRect:
// * draws a box of width, height at the given position
// * either fillStyle or strokeStyle is required
// optional parameters:
// * fillStyle: color string, hex value, or RGB value.
// * lineWidth: how thick is the line
// * strokeSytle: color in string ("blue"), a hex value (#FF0000), or rgb(255,0,0)
//
Engine.prototype.drawRect = function(leftX, topY, width, height, fillStyle, lineWidth, strokeStyle) {
	engineAssert( fillStyle || strokeStyle, "drawRect requires either a fillStyle or strokeStyle" );
	var ctx = this.ctx;
	ctx.save();

	if( fillStyle ) {
		ctx.fillStyle = fillStyle;
		ctx.fillRect( leftX, topY, width, height );
	}

	if( strokeStyle ) {
		if( lineWidth ) {
			ctx.lineWidth = lineWidth;
		}
		ctx.strokeStyle = strokeStyle;
		ctx.strokeRect(leftX, topY, width, height);
	}
	ctx.restore();
};

//////////////////////////////////////////////
// drawCircle:
// * draws a box of width, height at the given position
// * either fillStyle or strokeStyle is required
// optional parameters:
// * fillStyle: color string, hex value, or RGB value.
// * lineWidth: how thick is the line
// * strokeSytle: color in string ("blue"), a hex value (#FF0000), or rgb(255,0,0)
//
Engine.prototype.drawCircle = function(centerX, centerY, radius, fillStyle, lineWidth, strokeStyle) {
	engineAssert( fillStyle || strokeStyle, "drawRect requires either a fillStyle or strokeStyle" );
	var ctx = this.ctx;

	ctx.beginPath();
	ctx.arc(centerX, centerY, radius, 0, 2*Math.PI);
	if( fillStyle ) {
		ctx.fillStyle = fillStyle;
		ctx.fill();
	}

	if( strokeStyle ) {
		if( lineWidth ) {
			ctx.lineWidth = lineWidth;
		}
		ctx.strokeStyle = strokeStyle;
		ctx.stroke();
	}
};

// Test - try to change color of sprite by manipulating raw pixel
// To get this to work as I'd like, we would want the sprite drawn to an offscreen canvas and manipulate it there
// before copying the canvas over
//
// var data = ctx.getImageData(Math.floor(pos.x), Math.floor(pos.y), this.getAsset().width, this.getAsset().height);
// for (var i = 0, length = data.data.length; i < length; i += 4) {	// step over only red channel
//	data.data[i] = 0; // remove all red //Math.max(255, data.data[i]); // increase all red
// }
// ctx.putImageData(data, Math.floor(pos.x)+ 10, Math.floor(pos.y));
