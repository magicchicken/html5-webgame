
var AssertsEnabled = true;

(function() {

	if( AssertsEnabled ) {

		// Check the expression and use the console to error
		engineAssert = function(exp, message) {
			if( exp === undefined || exp === false ) {
				console.error(message);
			}
		};

	} else {
		// Empty function
		engineAssert = function(exp, message) { };
	}

})();


(function() {
	// Linear interpolation
	// on the straight and narrow
	Math.LinearInterp = function( t ) {
		return t;
	};
	
	// Quadratic-in interpolation
	// Ease-in behavior
	Math.QuadInInterp = function( t ) {
		return t * t;
	};

	// Quadratic-out interpolation
	// Ease-out behavior
	Math.QuadOutInterp = function( t ) {
		return 1 - ((1 - t) * (1 - t));
	};

	// S-curve interpolation
	// Ease in and out behavior
	Math.QuadSInterp = function( t ) {
		return 3.0 * (t * t) - 2.0 * (t * t * t);
	};
})();