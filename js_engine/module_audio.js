//*****************************************************************************
// Audio Module
//
// Required systems:
// * Engine
//
// Required libraries:
// * JQuery
//
// Features:
// * playSound( sound )
// * stopAllSounds( )
//
// Notes:
// This is pretty much the same as Dave's audio system, with the sound
// resource management pulled out and now uses the engine's asset manangment.
//*****************************************************************************

// Requires Engine namespace
(function() {
	engineAssert( Engine, "No Engine Definition" );
})();


Engine.Audio = function() {

	//-------------------------------------------------------------------------
	// shortcut reference to the engine context
	var engine = this;

	// storage for all active sounds
	var activeSounds = [];

	//-------------------------------------------------------------------------
	// Called when a sound is finished playing
	//
	function cleanActive( )
	{
		var i;
		for ( i = 0; i < activeSounds.length; ++i ) //Note: length changes
		{
			if ( activeSounds[ i ].ended )
			{
				activeSounds.splice( i, 1 );
			}
		}
	}

		 
	//-------------------------------------------------------------------------
	// Plays an audio file by name
	// Must have been loaded by the asset manager
	//
	function playByName( name, loop )
	{
		var audio = engine.getAsset( name );
		engineAssert(audio, "Could not find audio asset by name: " + name );
		if ( loop )
		{
			audio.loop = true;
		}
		$(audio).one( "ended", cleanActive );
		audio.play( );
		activeSounds.push( audio );
	}
		 
	//-------------------------------------------------------------------------
	// Plays a sequence of songs. One a song ends a callback will trigger
	// the next song
	//
	function playSequence( sound )
	{
		var index = 0,
			names = sound.names;
		if ( sound.random )
		{
			app.shuffleArray( names );
		}
		function playNext( )
		{
			var audio =  engine.getAsset( names[ index ] );
			++index;
			if ( sound.loop || (index < names.length) )
			{
				$(audio).one( "ended", playNext );
			}
			$(audio).one( "ended", cleanActive );
			audio.play( );
			activeSounds.push( audio );
			if ( index >= names.length && sound.loop )
				index = 0;
		}
		playNext( );
	}

	//=========================================================================
	// Plays a sound. Parameter can be a string or an object with multiple
	// sounds and parameters to play them
	//
	Engine.prototype.playSound = function( sound )
	{
		var r;
		if ( typeof sound === "string" )
		{
			playByName( sound );
		}
		else if ( typeof sound === "object" )
		{
			if ( sound.name )
			{
				playByName( sound.name, sound.loop );
			}
			else if ( sound.names )
			{
				if ( sound.type === "randomSelection" )
				{
					r = app.stdRandom.integer( sound.names.length );
					playByName( sound.names[ r ] );
				}
				else if ( sound.type === "sequence" )
				{
					playSequence( sound );
				}
			}
		}
	};

	//=========================================================================
	// Stops all active sounds
	//
	Engine.prototype.stopAllSounds = function( )
	{
		var i, lim;
		for ( i = 0, lim = activeSounds.length; i < lim; ++i )
		{
			activeSounds[ i ].pause( );
			activeSounds[ i ].currentTime = 0;
		}
		activeSounds = [];
	};
};