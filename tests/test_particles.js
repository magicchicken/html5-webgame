//------------------------------------------------
// Setup HTML using jquery
// Don't need if you already have html code written
//
$("body").append("<div id='fps'>0</div>");
$("body").append("<button id='restart'>Restart</button>");

$("#restart").on('click', function() {
	myEngine.stageScene("level");
});

//=========================================================================
// Gameplay Test Application
//=========================================================================
var engineConfig = {
	imagePath: "tests/images/",
	audioPath: "tests/audio/",
	dataPath:  "tests/data/"
};

//=========================================================================
// Create Engine Instance
//=========================================================================
var myEngine = Engine( engineConfig );
myEngine.includeModule("Sprites, Scenes, Animation, Particles" );

//=========================================================================
// Setup Canvas
//=========================================================================
myEngine.setupCanvas( );
myEngine.el.css('backgroundColor','#666');


//=========================================================================
// Setup Input
//=========================================================================


//=========================================================================
// Game Logic - Main Scene
//=========================================================================
function generator(stage) {

	myEngine.compileSheets('smoke.png','smoke.json');

	//=========================================================================
	// Object setup
	//
	// Our emitters are sprites with no asset or sheet name assigned, so they
	// will be invisible
	//
	var emitter = new Engine.Sprite( {x: -150, y:0 } );
	emitter.addComponent( "particleEmitter", {
		particleAssetName: "Star.png",
		debugDraw: true
	});
	stage.insert( emitter );


	var emitter2 = new Engine.Sprite( {x: 150, y:0 } );
	emitter2.addComponent( "particleEmitter", {
		particleSheetName: "smokeEffect",
		velocityRangeX: {min:-50, max:50 },
		velocityRangeY: {min:-50, max:-10},
		spawnRate: 0.1,
		spawnRange: {x:50, y:50},
		lifeTime: 2,
		alpha: function(props) {
			return 1.0 - Math.QuadOutInterp( props.time/props.lifeTime );
		},
		debugDraw: true
	});
	stage.insert( emitter2 );

	//=========================================================================
	// Camera - follow our new object
	//
	stage.addComponent("camera");
	stage.camera.centerViewportOn( 0, 0 );
}


var AssetList = [
	"Star.png",
	"smoke.png",
	"smoke.json"
];

var options = {
	sort: true
};

myEngine.addScene('level',new Engine.Scene(generator, options));

var options = {
	onFinishedCallback: function() {
		myEngine.stageScene("level");
	},

	onProgressCallback: function(index, total) {
		console.log( "asset progress: " + index + " / " + total );
	}
};

myEngine.load(AssetList, options );

var totalTime = 0,
	totalFrames = 0;
myEngine.setGameLoop(function(dt) {
	myEngine.stageGameLoop(dt);

	totalTime += dt;
	totalFrames += 1;
	$("#fps").text( Math.round(totalFrames / totalTime) + " FPS");
});