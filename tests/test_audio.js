//------------------------------------------------
// Setup HTML using jquery
// Don't need if you already have html code written
//
$("body").append("<div id='fps'>0</div>");
$("body").append("<button id='restart'>Restart</button>");

$("#restart").on('click', function() {
	myEngine.stageScene("level");
});

//=========================================================================
// Gameplay Test Application
//=========================================================================
var engineConfig = {
	imagePath: "tests/images/",
	audioPath: "tests/audio/",
	dataPath:  "tests/data/"
};

//=========================================================================
// Create Engine Instance
//=========================================================================
var myEngine = Engine( engineConfig );
myEngine.includeModule("Scenes, Audio" );

//=========================================================================
// Setup Canvas
//=========================================================================

//=========================================================================
// Setup Input
//=========================================================================


//=========================================================================
// Game Logic - Main Scene
//=========================================================================
function generator(stage) {

	//=========================================================================
	// Background Audio!
	//
	myEngine.stopAllSounds();
	myEngine.playSound( "John Kline - Take Five.ogg" );

}


var AssetList = [
	"John Kline - Take Five.ogg"
];


myEngine.addScene('level',new Engine.Scene(generator));

var options = {
	onFinishedCallback: function() {
		myEngine.stageScene("level");
	},

	onProgressCallback: function(index, total) {
		//appLoader.updateProgressMeter( )
		console.log( "asset progress: " + index + " / " + total );
	}
};

myEngine.load(AssetList, options );

var totalTime = 0,
	totalFrames = 0;
	
myEngine.setGameLoop(function(dt) {
	myEngine.stageGameLoop(dt);

	totalTime += dt;
	totalFrames += 1;
	$("#fps").text( Math.round(totalFrames / totalTime) + " FPS");
});
