//------------------------------------------------
// Setup HTML using jquery
// Don't need if you already have html code written
//
$("body").append("<div id='fps'>0</div>");
$("body").append("<button id='restart'>Restart</button>");

$("#restart").on('click', function() {
	myEngine.stopAllSounds();
	myEngine.stageScene("level");
});

//=========================================================================
// Gameplay Test Application
//=========================================================================
var engineConfig = {
	imagePath: "tests/images/",
	audioPath: "tests/audio/",
	dataPath:  "tests/data/"
};

//=========================================================================
// Create Engine Instance
//=========================================================================
var myEngine = Engine( engineConfig );
myEngine.includeModule("Input, Sprites, Scenes, Physics, Animation, Audio, Tilemap, Particles" );

//=========================================================================
// Setup Canvas
//=========================================================================
myEngine.setupCanvas( );
myEngine.el.css('backgroundColor','#666');

//=========================================================================
// Setup Input
//=========================================================================
myEngine.input.enableMouse();
myEngine.input.enableKeyboard();
myEngine.input.setKeyboardControls(); // use defaults

//=========================================================================
// Game Logic - Main Scene
//=========================================================================
function generator(stage) {

	var tiles = {
		"stone": {
			assetName: "Stone Block.png",
			collision: false
		},
		"plain": {
			assetName: "Plain Block.png",
			collision: false
		},
		"water": {
			assetName: "Water Block.png",
			collision: true
		},
		"dirt": {
			assetName: "Dirt Block.png",
			collision: false
		},
		"grass": {
			assetName: "Grass Block.png",
			collision: false
		},
		"wall": {
			assetName:"Wall Block.png",
			collision: false
		},
		"wood": {
			assetName:"Wood Block.png",
			collision: false
		}
	};

	var map = {
		numRows: 5,
		numColums: 7,
		numLayers: 5,

		layerOffset: 40,
		tileOffset: { x: 101, y:82 },

		tiles: [
			{ row: 0, col: 0, layer: 3, tileName: "stone" },
			{ row: 0, col: 1, layer: 3, tileName: "stone" },
			{ row: 0, col: 2, layer: 3, tileName: "stone" },
			{ row: 0, col: 3, layer: 3, tileName: "stone" },
			{ row: 0, col: 4, layer: 3, tileName: "plain" },
			{ row: 0, col: 5, layer: 3, tileName: "plain" },
			{ row: 0, col: 6, layer: 3, tileName: "plain" },

			{ row: 0, col: 5, layer: 4, tileName: "wood" },
			{ row: 0, col: 6, layer: 4, tileName: "wood" },

			{ row: 1, col: 0, layer: 3, tileName: "stone" },
			{ row: 1, col: 1, layer: 3, tileName: "stone" },
			{ row: 1, col: 2, layer: 3, tileName: "water" },
			{ row: 1, col: 3, layer: 3, tileName: "water" },
			{ row: 1, col: 4, layer: 3, tileName: "plain" },
			{ row: 1, col: 5, layer: 3, tileName: "plain" },
			{ row: 1, col: 6, layer: 3, tileName: "plain" },

			{ row: 2, col: 0, layer: 3, tileName: "stone" },
			{ row: 2, col: 1, layer: 3, tileName: "stone" },
			{ row: 2, col: 2, layer: 3, tileName: "water" },
			{ row: 2, col: 3, layer: 3, tileName: "water" },
			{ row: 2, col: 4, layer: 3, tileName: "plain" },
			{ row: 2, col: 5, layer: 3, tileName: "plain" },
			{ row: 2, col: 6, layer: 3, tileName: "plain" },

			{ row: 3, col: 0, layer: 3, tileName: "grass" },
			{ row: 3, col: 1, layer: 3, tileName: "grass" },
			{ row: 3, col: 2, layer: 3, tileName: "water" },
			{ row: 3, col: 3, layer: 3, tileName: "water" },
			{ row: 3, col: 4, layer: 3, tileName: "grass" },
			// { row: 3, col: 5, layer: 3, tileName: "dirt" },
			// { row: 3, col: 6, layer: 3, tileName: "dirt" },

			{ row: 4, col: 0, layer: 3, tileName: "grass" },
			{ row: 4, col: 1, layer: 3, tileName: "grass" },
			{ row: 4, col: 2, layer: 3, tileName: "water" },
			{ row: 4, col: 3, layer: 3, tileName: "water" },
			{ row: 4, col: 4, layer: 3, tileName: "grass" },
			// { row: 4, col: 5, layer: 3, tileName: "dirt" },
			// { row: 4, col: 6, layer: 3, tileName: "dirt" },

			{ row: 3, col: 0, layer: 2, tileName: "grass" },
			{ row: 3, col: 1, layer: 2, tileName: "grass" },
			{ row: 3, col: 2, layer: 2, tileName: "grass" },
			{ row: 3, col: 3, layer: 2, tileName: "stone" },
			{ row: 3, col: 4, layer: 2, tileName: "stone" },
			{ row: 3, col: 5, layer: 2, tileName: "dirt" },
			{ row: 3, col: 6, layer: 2, tileName: "dirt" },

			{ row: 4, col: 0, layer: 2, tileName: "grass" },
			{ row: 4, col: 1, layer: 2, tileName: "grass" },
			{ row: 4, col: 2, layer: 2, tileName: "grass" },
			{ row: 4, col: 3, layer: 2, tileName: "stone" },
			{ row: 4, col: 4, layer: 2, tileName: "stone" },
			{ row: 4, col: 5, layer: 2, tileName: "dirt" },
			{ row: 4, col: 6, layer: 2, tileName: "dirt" }
		]
	};

	//=========================================================================
	// Object setup
	//
	var props = {
		tileData: tiles,
		mapData: map,
		debugDraw: true
	};
	var tilemap = new Engine.TileLayer(props);
	stage.insert( tilemap );


	var playerProps = {
		assetName: "Character Boy.png",
		x: 0,
		y: 0,
		z: 100
	};
	var newPlayer = new Engine.Sprite( playerProps );
	stage.insert( newPlayer );

	//=========================================================================
	// Camera - follow our new object
	//
	stage.addComponent("camera");
	stage.camera.followEntity( newPlayer );

	//=========================================================================
	// Input tests
	//
	var mouseDown = false;
	var mouseLastPos = null;

	myEngine.input.bindEvent( "mousewheel", stage, function(wheelDelta) {
		var zoomInc = 0.1;
		if( wheelDelta > 0 ) {
			stage.properties.scale += zoomInc;
		} else {
			stage.properties.scale -= zoomInc;
		}
	});

	//=========================================================================
	// Quick hack for player movement
	//
	var MoveSpeed = 10;
	var moveDirection = {x: 0, y: 0 };
	myEngine.input.bindEvent( "left", stage, function() {
		moveDirection.x = -1;
	});
	myEngine.input.bindEvent( "leftUp", stage, function() {
		moveDirection.x = 0;
	});

	myEngine.input.bindEvent( "right", stage, function() {
		moveDirection.x = 1;
	});
	myEngine.input.bindEvent( "rightUp", stage, function() {
		moveDirection.x = 0;
	});

	myEngine.input.bindEvent( "up", stage, function() {
		moveDirection.y = -1;
	});
	myEngine.input.bindEvent( "upUp", stage, function() {
		moveDirection.y = 0;
	});
	
	myEngine.input.bindEvent( "down", stage, function() {
		moveDirection.y = 1;
	});
	myEngine.input.bindEvent( "downUp", stage, function() {
		moveDirection.y = 0;
	});
	newPlayer.step = function(dt) {
		this.properties.x += moveDirection.x * MoveSpeed;
		this.properties.y += moveDirection.y * MoveSpeed;
	};
}


var AssetList = [
	"Character Boy.png",

	"Brown Block.png",
	"Dirt Block.png",
	"Grass Block.png",
	"Plain Block.png",
	"Stone Block Tall.png",
	"Stone Block.png",
	"Wall Block Tall.png",
	"Wall Block.png",
	"Water Block.png",
	"Wood Block.png"
];

var options = {
	sort: true
};

myEngine.addScene('level',new Engine.Scene(generator, options));

var options = {
	onFinishedCallback: function() {
		myEngine.stageScene("level");
	},

	onProgressCallback: function(index, total) {
		//appLoader.updateProgressMeter( )
		console.log( "asset progress: " + index + " / " + total );
	}
};

myEngine.load(AssetList, options );

var totalTime = 0,
	totalFrames = 0;
myEngine.setGameLoop(function(dt) {
	myEngine.stageGameLoop(dt);

	totalTime += dt;
	totalFrames += 1;
	$("#fps").text( Math.round(totalFrames / totalTime) + " FPS");
});

// var AssetList = [
// 	"wood-pattern.png",

// 	"Character Boy.png",
// 	"Character Cat Girl.png",
// 	"Character Horn Girl.png",
// 	"Character Pink Girl.png",
// 	"Character Princess Girl.png",

// 	"Chest Closed.png",
// 	"Chest Lid.png",
// 	"Chest Open.png",

// 	"Door Tall Closed.png",
// 	"Door Tall Open.png",

// 	"Enemy Bug.png",

// 	"Selector.png",
// 	"Gem Blue.png",
// 	"Gem Green.png",
// 	"Gem Orange.png",
// 	"Rock.png",

// 	"Brown Block.png",
// 	"Dirt Block.png",
// 	"Grass Block.png",
// 	"Plain Block.png",
// 	"Wall Block Tall.png",
// 	"Wall Block.png",
// 	"Water Block.png",
// 	"Wood Block.png",

// 	"Heart.png",
// 	"Key.png",
// 	"Ramp East.png",
// 	"Ramp North.png",
// 	"Ramp South.png",
// 	"Ramp West.png",

// 	"Roof East.png",
// 	"Roof North East.png",
// 	"Roof North West.png",
// 	"Roof South East.png",
// 	"Roof South West.png",
// 	"Roof South.png",
// 	"Roof West.png",

// 	"Shadow East.png",
// 	"Shadow North East.png",
// 	"Shadow North West.png",
// 	"Shadow North.png",
// 	"Shadow Side West.png",
// 	"Shadow South East.png",
// 	"Shadow South West.png",
// 	"Shadow South.png",
// 	"Shadow West.png",

// 	"SpeechBubble.png",
// 	"Star.png",
// 	"Stone Block Tall.png",
// 	"Stone Block.png",
// 	"Tree Short.png",
// 	"Tree Tall.png",
// 	"Tree Ugly.png",
// 	"Window Tall.png"
// ];